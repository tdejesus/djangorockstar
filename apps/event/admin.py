# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Portal)
admin.site.register(models.Event)
admin.site.register(models.FAQ)
admin.site.register(models.Coach)
admin.site.register(models.Organizer)
admin.site.register(models.Sponsor)
admin.site.register(models.ProgramingLevel)
admin.site.register(models.OperatingSystem)
admin.site.register(models.Participant)
admin.site.register(models.Languages)
