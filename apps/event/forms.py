# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from django.forms import CheckboxSelectMultiple
from .models import Participant, Coach, OperatingSystem


class ParticipantForm(forms.ModelForm):
    class Meta:
        model = Participant
        fields = [
            'name', 'email', 'phone_number', 'city', 'birthdate',
            'programing_knowledge', 'current_ocupation', 'reason_assistance',
            'event', 'operation_system', 'programing_level']


class CoachForm(forms.ModelForm):
    class Meta:
        model = Coach
        fields = ('name', 'email', 'phone_number', 'github_user',
                  'operating_systems', 'agree_work_with_windows',
                  'languages_speak', 'time_install_before_workshop',
                  'time_meet_before_workshop', 'experience_teaching',
                  'want_be_add_website', 'picture', 'event')

    # operating_systems = forms.ModelMultipleChoiceField(
    #     queryset=OperatingSystem.objects.all(),
    #     widget=CheckboxSelectMultiple,
    #     required=False, help_text='')

    # name = forms.CharField(required=True,
    #                        label="Cuál es tu nombre?",
    #                        max_length=64,
    #                        widget=forms.TextInput(attrs={'class':
    #                                                      "form-control"}),)

    # email = forms.CharField(required=True,
    #                         label="Cuál es tu correo electrónico?",
    #                         max_length=120,
    #                         widget=forms.TextInput(attrs={'class':
    #                                                       "form-control"}),)
