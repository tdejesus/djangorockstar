# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
import uuid


class Portal(models.Model):
    name = models.CharField(max_length=64)
    about_us = models.TextField()
    title_aplication_session = models.CharField(max_length=64)
    text_aplication_session = models.TextField()
    subtext_aplication_session = models.TextField(blank=True, null=True)
    title_coach_session = models.CharField(max_length=64)
    subtitle_coach_session = models.CharField(max_length=120)
    text_coach_session = models.TextField()
    title_organizer_session = models.CharField(max_length=64)
    subtitle_organizer_session = models.CharField(
        max_length=120, blank=True, null=True)
    title_sponsor_session = models.CharField(max_length=64)
    subtitle_sponsor_session = models.CharField(max_length=120,
                                                null=True, blank=True)
    participant_sucess = models.TextField()
    participant_warning = models.TextField()
    coach_success = models.TextField()
    btn_inscription = models.CharField(max_length=30)
    text_form_inscription_participant = models.TextField(null=True, blank=True)
    text_form_inscription_sponsor = models.TextField(null=True, blank=True)

    text_footer = models.CharField(max_length=120, null=True, blank=True)

    picture_slide = models.ImageField(
        upload_to="portal", null=True, blank=True)
    picture_1_application = models.ImageField(
        upload_to="portal", null=True, blank=True)
    picture_2_application = models.ImageField(
        upload_to="portal", null=True, blank=True)
    picture_bg_coach = models.ImageField(
        upload_to="portal", null=True, blank=True)

    logo = models.ImageField(
        upload_to="portal", null=True, blank=True)

    def __unicode__(self):
        return '{}'.format(self.name)


class Event(models.Model):

    # Fields
    portal = models.ForeignKey(Portal)
    name = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    about = models.TextField()
    begin_date = models.DateTimeField()
    end_date = models.DateTimeField()
    place = models.CharField(max_length=40)
    location = models.CharField(max_length=30)
    is_active = models.BooleanField(default=False)
    max_participantes = models.IntegerField(default=0)
    token = models.UUIDField(primary_key=True,
                             default=uuid.uuid4,
                             editable=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.name


class OperatingSystem(models.Model):

    # Fields
    name = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.name


class ProgramingLevel(models.Model):

    # Fields
    name = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('id',)

    def __unicode__(self):
        return u'%s' % self.name


class Participant(models.Model):

    # Fields
    name = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    email = models.EmailField()
    phone_number = models.CharField(max_length=16)
    city = models.CharField(max_length=32)
    birthdate = models.CharField(max_length=2, null=True, blank=True)
    programing_knowledge = models.TextField(null=True, blank=True)
    current_ocupation = models.TextField()
    reason_assistance = models.TextField()
    picture = models.ImageField(upload_to="participant", null=True)

    # Relationship Fields
    event = models.ForeignKey(Event, related_name='participants')
    operation_system = models.ForeignKey(OperatingSystem)
    programing_level = models.ForeignKey(ProgramingLevel)
    is_assistance_confirmated = models.BooleanField(default=False)
    is_max_capacity_out = models.BooleanField(default=False)
    token = models.UUIDField(primary_key=True,
                             default=uuid.uuid4,
                             editable=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.name


class Languages(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return u'%s' % self.name


class Coach(models.Model):

    YES_OR_NO_CHOICES = (
        ('', '-------------'),
        ('Si', 'Si'),
        ('No', 'No'),
    )

    # Fields
    name = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    email = models.EmailField()
    phone_number = models.CharField(max_length=20)
    github_user = models.CharField(max_length=120, blank=True, null=True)
    agree_work_with_windows = models.CharField(
        max_length=2,
        choices=YES_OR_NO_CHOICES,
        default="KG"
    )
    languages_speak = models.ManyToManyField(Languages)
    time_install_before_workshop = models.CharField(
        max_length=2,
        choices=YES_OR_NO_CHOICES,
        default="KG"
    )
    time_meet_before_workshop = models.CharField(
        max_length=2,
        choices=YES_OR_NO_CHOICES,
        default="KG"
    )
    want_be_add_website = models.CharField(
        max_length=2,
        choices=YES_OR_NO_CHOICES,
        default="KG"
    )
    experience_teaching = models.TextField(blank=True, null=True)
    picture = models.ImageField(upload_to="coach", blank=True, null=True)

    # Relationship Fields
    event = models.ForeignKey(Event, related_name='coaches')
    operating_systems = models.ManyToManyField(OperatingSystem)
    token = models.UUIDField(primary_key=True,
                             default=uuid.uuid4,
                             editable=False)
    is_verified = models.BooleanField(default=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.name


class Organizer(models.Model):

    # Fields
    name = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    email = models.EmailField()
    phone_number = models.CharField(max_length=20)
    picture = models.ImageField(upload_to="participant")

    # Relationship Fields
    event = models.ForeignKey(Event, related_name='organizers')

    def __unicode__(self):
        return '{}'.format(self.name)


class Sponsor(models.Model):

    # Fields
    name = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    email = models.EmailField()
    phone_number = models.CharField(max_length=20)
    link_page = models.CharField(max_length=255)
    picture = models.ImageField(upload_to="participant")

    # Relationship Fields
    event = models.ForeignKey(Event, related_name='sponsors')

    def __unicode__(self):
        return '{}'.format(self.name)


class FAQ(models.Model):

    portal = models.ForeignKey(Portal, related_name='faqs')
    question = models.CharField(max_length=120)
    answer = models.TextField()

    def __unicode__(self):
        return '{}'.format(self.question)
