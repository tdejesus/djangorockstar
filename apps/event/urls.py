from django.conf.urls import url
from . import views

urlpatterns = [
    # PARTIPANT
    url(r'^(?P<event_pk>[0-9A-Fa-f-]+)/participant/$',
        views.ParticipantCreateView.as_view(), name="participant"),
    url(r'^(?P<pk>[0-9A-Fa-f-]+)/participant/success/$',
        views.ParticipantSuccessView.as_view(), name="participant_success"),
    url(r'^(?P<pk>[0-9A-Fa-f-]+)/participant/warning/$',
        views.ParticipantWarningView.as_view(), name="participant_warning"),

    # COACH
    url(r'^(?P<event_pk>[0-9A-Fa-f-]+)/coach/$',
        views.CoachCreateView.as_view(), name="coach"),
    url(r'^(?P<pk>[0-9A-Fa-f-]+)/coach/success/$',
        views.CoachSuccessView.as_view(), name="coach_success"),
]
