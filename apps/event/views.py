# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic import CreateView, TemplateView, DetailView
from django.urls.base import reverse
from . import models, forms


class ParticipantCreateView(CreateView):
    form_class = forms.ParticipantForm
    model = models.Participant

    def get_context_data(self, **kwargs):
        context = super(ParticipantCreateView, self).get_context_data(**kwargs)
        context['event'] = models.Event.objects.get(
            pk=self.kwargs.get('event_pk'))
        return context

    def post(self, request, *args, **kwargs):
        request_post = request.POST.copy()
        request_post.update({'event': kwargs.get('event_pk')})
        request.POST = request_post
        return super(
            ParticipantCreateView, self).post(request, *args, **kwargs)

    def get_success_url(self, **kwargs):
        event = models.Event.objects.get(pk=self.kwargs.get('event_pk'))
        if event.max_participantes > event.participants.count():
            return reverse('participant_success',
                           kwargs={'pk': self.object.pk})
        else:
            self.object.is_max_capacity_out = True
            self.object.save()
            return reverse('participant_warning',
                           kwargs={'pk': self.object.pk})


class ParticipantSuccessView(DetailView):
    template_name = 'event/participant_success.html'
    model = models.Participant

    def get_context_data(self, **kwargs):
        context = super(
            ParticipantSuccessView, self).get_context_data(**kwargs)
        context['event'] = self.get_object().event
        return context


class ParticipantWarningView(DetailView):
    template_name = 'event/participant_warning.html'
    model = models.Participant

    def get_context_data(self, **kwargs):
        context = super(
            ParticipantWarningView, self).get_context_data(**kwargs)
        context['event'] = self.get_object().event
        return context


class CoachCreateView(CreateView):
    form_class = forms.CoachForm
    model = models.Coach

    def get_context_data(self, **kwargs):
        context = super(CoachCreateView, self).get_context_data(**kwargs)
        context['event'] = models.Event.objects.get(
            pk=self.kwargs.get('event_pk'))
        return context

    def post(self, request, *args, **kwargs):
        request_post = request.POST.copy()
        request_post.update({'event': kwargs.get('event_pk')})
        request.POST = request_post
        return super(CoachCreateView, self).post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('coach_success', kwargs={'pk': self.object.pk})


class CoachSuccessView(DetailView):
    template_name = 'event/coach_success.html'
    model = models.Coach

    def get_context_data(self, **kwargs):
        context = super(CoachSuccessView, self).get_context_data(**kwargs)
        context['event'] = self.get_object().event
        return context


class CoachTemplateView(TemplateView):
    template_name = 'event/coach_success.html'
