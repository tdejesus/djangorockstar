# -*- coding: utf-8 -*-
from .base import *


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': get_env_variable('MYSQL_DB'),
        'USER': get_env_variable('MYSQL_USER'),
        'PASSWORD': get_env_variable('MYSQL_PWD'),
        'HOST': get_env_variable('MYSQL_HOST'),
        'PORT': get_env_variable('MYSQL_PORT'),
        'OPTIONS': {
            'sql_mode': 'traditional',
        }
    }
}

INTERNAL_IPS = '127.0.0.1'
